package id.ac.ub.papb.intent1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button bt1, bt2;
    EditText et1, et2;

    public static final String NAMA = "nama";
    public static final String ALAMAT = "alamat";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt1 = findViewById(R.id.bt1);
        bt1.setOnClickListener(this);
        bt2 = findViewById(R.id.bt2);
        bt2.setOnClickListener(this);
// cara ke 2
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("http://filkom.ub.ac.id");
                Intent in = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(in);
            }
        });
//end of cara 2

//cara 3
        View.OnClickListener l2 = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("http://filkom.ub.ac.id");
                Intent in = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(in);
            }
        };
        bt2.setOnClickListener(l2);
// end of cara 3

        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.bt1) {
            Intent in = new Intent(this, Activity2.class);
            Bundle b = new Bundle();
            b.putString(NAMA, et1.getText().toString());
            b.putString(ALAMAT, et2.getText().toString());
            in.putExtras(b);
            startActivity(in);
        } else if (view.getId() == R.id.bt2) {
            Uri uri = Uri.parse("http://filkom.ub.ac.id");
            Intent in = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(in);
        }
    }
}