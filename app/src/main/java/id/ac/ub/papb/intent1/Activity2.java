package id.ac.ub.papb.intent1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    TextView tv1, tv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);

        Intent in=getIntent();
        Bundle b=in.getExtras();
        String nama=b.getString(MainActivity.NAMA);
        String alamat=b.getString(MainActivity.ALAMAT);

        tv1.setText(nama);
        tv2.setText(alamat);
    }
}